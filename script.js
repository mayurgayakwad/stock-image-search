let input = document.querySelector(".input")
let searchButton = document.querySelector(".searchbutton")
let imagesToAdd = document.querySelector(".addingImages")


function imageFetching(search){
    imagesToAdd.innerHTML =""
    let url = `https://api.unsplash.com/search/photos?page=1&query=${search || "cars"}&client_id=3DbnkAyP881aRa6UdwtwEpm9UbrvKOE6OkGPddYJr2c`
    fetch(url)
    .then((response) => response.json())
    .then((data) =>data.results.forEach(element => {
        let div1 = document.createElement("div")
        div1.className = "imagesDiv"
        div1.style.width = "500px"
        div1.style.backgroundSize = "100% 100%"
        div1.style.height = "300px"
        div1.style.backgroundImage =  `url(${element.urls.regular})`
        console.log(element.urls.regular)
        imagesToAdd.appendChild(div1)
    }))
}
imageFetching()

input.addEventListener("keyup", (e) => {    
    if (e.key == "Enter") {
       imageFetching(input.value)
    }
});
searchButton.addEventListener("click", (e) => {
       imageFetching(input.value)
});